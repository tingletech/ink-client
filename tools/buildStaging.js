import { STAGING_DIST_DIR } from './build-constants';
import { runWebpackWithEnv } from './buildShared';

process.env.NODE_ENV = 'production'; // this assures React is built in staging ("prod") mode and that the Babel dev config doesn't apply.
process.env.STAGE = 'staging';

runWebpackWithEnv(STAGING_DIST_DIR);