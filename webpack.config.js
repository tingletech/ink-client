// For info about this file refer to webpack and webpack-hot-middleware documentation
// Rather than having hard coded webpack.config.js for each environment, this
// file generates a webpack config for the environment passed to the getConfig method.
import webpack from 'webpack';
import path from 'path';
import ExtractTextPlugin from 'extract-text-webpack-plugin';
import { DEVELOPMENT, PRODUCTION, STAGING, TEST, DEMO } from './src/constants/Environments';
import { PRODUCTION_DIST_DIR, STAGING_DIST_DIR, DEMO_DIST_DIR } from './tools/build-constants';

const developmentEnvironment = DEVELOPMENT ;
const productionEnvironment = PRODUCTION;
const stagingEnvironment = STAGING;
const demoEnvironment = DEMO;
const testEnvironment = TEST;

const getStage = function(env) {
  if(env == DEVELOPMENT) {
    return(JSON.stringify(DEVELOPMENT));
  } else {
    return(JSON.stringify(process.env.STAGE));
  }
};

const getPlugins = function (env) {
  const stage = getStage();
  const GLOBALS = {
    'process.env.NODE_ENV': JSON.stringify(env),
    'process.env.STAGE': getStage(),
    __DEV__: env === developmentEnvironment,
    __PROD__: stage === productionEnvironment,
    __STAGING__: stage === stagingEnvironment,
    __DEMO__: stage === demoEnvironment
  };

  const plugins = [
    new webpack.optimize.OccurenceOrderPlugin(),
    new webpack.DefinePlugin(GLOBALS) //Tells React to build in prod mode. https://facebook.github.io/react/downloads.html
  ];

  if(productionLikeBuild(env)) {
    plugins.push(new ExtractTextPlugin('/styles.css'));
    plugins.push(new webpack.optimize.DedupePlugin());
    plugins.push(new webpack.optimize.UglifyJsPlugin());
  } else {
    plugins.push(new webpack.HotModuleReplacementPlugin());
    plugins.push(new webpack.NoErrorsPlugin());
  }

  return plugins;
};

const getEntry = function (env) {
  const entry = [];

  if (env == DEVELOPMENT) { // only want hot reloading when in dev.
    entry.push('webpack-hot-middleware/client');
  }
  entry.push('./src/index');

  return entry;
};

const getLoaders = function (env) {
  const loaders = [
    { test: /\.js$/, include: path.join(__dirname, 'src'), loaders: ['babel', 'eslint'] },
    { test: /\.json$/, loader: 'json'},
    { test: /fonts\/.*\.(woff|woff2|eot|ttf|svg)$/, loader: 'file-loader?name="[name]-[hash].[ext]"'},
    // { test: /\.(woff|woff2|eot|ttf|svg)$/, loader: 'file-loader?name="[name]-[hash].[ext]"'},
    { test: /\.(scss)$/, loaders: ["style", "css", "sass"] },
    { test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "url-loader?limit=10000&mimetype=application/font-woff" },
    { test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "file-loader" },
    { test: /\.yaml$/, loader: 'yaml', }

    // { test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "url-loader?limit=10000&mimetype=application/font-woff" },
    // { test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: "file-loader" },
    // { test: /\.(eot|svg|ttf|woff|woff2)$/, loader: 'file?name=public/fonts/[name].[ext]'},
    // { test: /\.(png|woff|woff2|eot|ttf|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: 'url'}
  ];

  if (env == PRODUCTION) {
    // generate separate physical stylesheet for production build using ExtractTextPlugin. This provides separate caching and avoids a flash of unstyled content on load.
    loaders.push({test: /\.(css|scss|sass)$/, loader: ExtractTextPlugin.extract("css!sass")});
  } else {
    loaders.push({test: /\.(css|scss|sass)$/, loaders: ['style', 'css', 'sass']});
  }

  return loaders;
};

function outputPath() {
  let stage = process.env.STAGE;
  if(stage == PRODUCTION) {
    return(`/${PRODUCTION_DIST_DIR}`);
  } else if(stage == STAGING) {
    return(`/${STAGING_DIST_DIR}`);
  } else if(stage == DEMO) {
    return(`/${DEMO_DIST_DIR}`);
  }
}

function productionLikeBuild(env) {
  return(env == PRODUCTION || env == STAGING || env == DEMO);
}

function getConfig(env) {
  return {
    debug: true,
    devtool: productionLikeBuild(env) ? 'source-map' : 'cheap-module-eval-source-map', // more info:https://webpack.github.io/docs/build-performance.html#sourcemaps and https://webpack.github.io/docs/configuration.html#devtool
    noInfo: true, // set to false to see a list of every file being bundled.
    entry: getEntry(env),
    target: 'web', // necessary per https://webpack.github.io/docs/testing.html#compile-and-test
    output: {
      path: __dirname + outputPath(), // Note: Physical files are only output by the production build task `npm run build` for production-like environments (e.g. staging and demo).
      publicPath: env == DEVELOPMENT ? 'http://localhost:3000/' : '/',
      filename: 'bundle.js'
    },
    plugins: getPlugins(env),
    module: {
      loaders: getLoaders(env)
    }
  };
}

export default getConfig;
