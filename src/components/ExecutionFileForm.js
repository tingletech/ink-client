import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, compose } from 'redux';
import { reduxForm, Field, Form, formValueSelector } from 'redux-form';
import Dropzone from 'react-dropzone';
import * as actions from '../actions/recipeActions';
import _ from 'lodash';

import ParameterSummary from './execution/ParameterSummary.js';
import * as viewConstants from '../constants/Views';

const FILE_FIELD_NAME = 'input_files';

const renderUploadFileList = (files) => {
  if(files && Array.isArray(files)) {
    return(
      <ul className="dropzone-file-list">
        { files.map((file, i) =>
          <li key={i} className="dropzone-file-list-item">
            <span className="fa fa-file-o"/> {file.name}
          </li>
        )}
      </ul>
    );
  } else {
    return(<div className="left-indent small-info"><span className="fa fa-hand-o-left"/> Please select some</div>);
  }
}

const renderDropzoneInput = (field) => {
  const files = field.input.value;
  return (
    <div className="dropzone-container">
      <div className="dropzone-widget">
        <Dropzone
          name={field.name}
          onDrop={( filesToUpload, e ) => field.input.onChange(filesToUpload)}
        >
          <div className="dropzone">Drop files here, or click to select files.</div>
          <div className="dropzone-icon-container">
            <span className="dropzone-icon fa fa-files-o"/>
          </div>
        </Dropzone>
        {field.meta.touched &&
          field.meta.error &&
          <span className="error">{field.meta.error}</span>}
      </div>
      <div className="dropzone-file-list">
        <div className="dropzone-file-list-title left-indent"><span className="fa fa-files-o"/> Files to upload ({files.length})</div>
        { renderUploadFileList(files) }
      </div>
    </div>
  );
};

class ExecutionFileForm extends Component {

  onSubmit(data) {
    const { dispatch, appState } = this.props;

    let formData = new FormData();
    formData.append('execution_parameters', JSON.stringify(this.compileResults()));
    _.map(data.input_files, function(file) {
     formData.append('input_files[]', file);
    });
    const { signedIn, authToken, tokenType, client, expiry, uid } = appState.session;
    dispatch(actions.executeRecipe(this.props.recipe.id, formData, signedIn, authToken, tokenType, client, expiry, uid));
  }

  compileResults() {
    let { state, recipe } = this.props;
    let compiledResults = {};
    let thisContext = this;
    _.map(recipe.recipe_steps, function(step) {
      let params = {};
      params.data = thisContext.assembleParameters(step, state);
      compiledResults[step.position] = params;
    });
    console.log("Parameters:", compiledResults)
    return compiledResults;
    // execution_parameters: { "1" => {data: { "animal" => "honey badger", "abc" => "2.5" }} },
  }

  assembleParameters(step, state) {
    let position = step.position;
    let newSet = {};

    if(step.parameterView == viewConstants.SHOW_PARAMETER_FORM) {
      // if param view is showing
      const selector = formValueSelector(`executionParameterForm_${position}`);
      let params = selector(state, 'parameters');
      let whatever = this;
      _.forEach(params, function(paramSet) {
        newSet[paramSet.key] = paramSet.value;
      });
    } else if(step.parameterView == viewConstants.SHOW_PRESET_VIEW) {
      // if preset view is showingPresetView
      if(!_.isEmpty(step.selectedPreset)) {
        return step.selectedPreset.execution_parameters;
      }
    }
    return newSet;
  }

  executionButton(inProgress) {
    if(inProgress) {
      return(
        <button type="submit" className="file-button" disabled>
          Starting... <span className="fa fa-gear fa-spin fa-fw" />
        </button>
      );
    }
    else {
      return(
        <button type="submit" className="file-button">
          GO!
        </button>
      );
    }
  }

  render() {
    const {
      handleSubmit,
      reset,
    } = this.props;
    return (
      <Form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
        <div className="execution-action-container">
          <div>
            <Field
              name={FILE_FIELD_NAME}
              component={renderDropzoneInput}
            />
          </div>
          <div className="parameter-summary-container">
            <div className="parameter-summary-title">
              <span className="fa fa-key"/> Execution Parameters
            </div>
            <ParameterSummary recipe={this.props.recipe} />
          </div>
          <div className="padding-top-1rem">
            { this.executionButton(this.props.appState.executionPlaceholderCount >= 1) }
          </div>
        </div>
      </Form>
    );
  }
}

ExecutionFileForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  reset: PropTypes.func.isRequired,
  recipe: PropTypes.object.isRequired,
  dispatch: PropTypes.func.isRequired,
  appState: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {
    appState: state.appState,
    state: state
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch),
    dispatch: dispatch
  };
}

export default compose(
  connect(mapStateToProps, mapDispatchToProps),
  reduxForm({form: "executeRecipe"})
)(ExecutionFileForm);
