import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators, compose } from 'redux';

import * as actions from '../../actions/recipeActions.js';

import _ from 'lodash';
import Dropzone from 'react-dropzone';
import { Field, reduxForm, Form, formValueSelector } from 'redux-form';
import validateSandbox from './ValidateSandbox';

import SandboxParameterForm from './SandboxParameterForm';

class CodeForm extends Component {
  onSubmit(data) {
    const { dispatch, appState } = this.props;
    let formData = new FormData();

    let sandboxParams = {};
    sandboxParams.step_class_name = data.step_class_name;
    sandboxParams.description = data.description;
    sandboxParams.execution_parameters = this.assembleParameters(this.props.state);

    formData.append("single_step_execution", JSON.stringify(sandboxParams));

    _.map(data.input_files, function(file) {
     formData.append('input_files[]', file);
    });
    formData.append('code', data.code);

    const { signedIn, authToken, tokenType, client, expiry, uid } = appState.session;
    dispatch(actions.executeSandbox(formData, signedIn, authToken, tokenType, client, expiry, uid));
  }

  assembleParameters(state) {
    let newSet = {};

    const selector = formValueSelector(`sandboxParameterForm`);
    let params = selector(state, 'parameters');

    _.forEach(params, function(paramSet) {
      newSet[paramSet.key] = paramSet.value;
    });
    return newSet;
  }

  listExpandCollapseToggle(singleStepExecution) {
    return(
      <a href="#" onClick={e => this.handleToggle(e, singleStepExecution.file_list_collapse, singleStepExecution.id, singleStepExecution.process_chain_id, this.props.recipeId)}>
        <span className={this.listIcon(singleStepExecution.file_list_collapse)}/>
        &nbsp;Files ({_.size(singleStepExecution.output_file_manifest)})
      </a>
    );
  }

  listIcon(collapsed) {
    if(collapsed == true || _.isNil(collapsed)) {
      return "fa fa-caret-right";
    } else {
      return "fa fa-caret-down";
    }
  }

  successClass(execution_errors, successful) {
    if(successful === true) {
      return("success success-background");
    }
    else if(!_.isNil(execution_errors) && !_.isEmpty(execution_errors)) {
      return("fail fail-background");
    }
    return("");
  }

  renderIcon(singleStepExecution) {
    if(singleStepExecution.in_progress === true) {
      return(<span className="fa fa-gear fa-spin fa-2x fa-fw step-processing-centered" />);
    }
    else if(_.isNil(singleStepExecution.started_at)) {
      return(<span className="small-info-text fa fa-question"/>);
    }
    else if(singleStepExecution.successful === true) {
      return(<span className="fa fa-check"/>);
    }
    else if(!_.isNil(singleStepExecution.execution_errors) && !_.isEmpty(singleStepExecution.execution_errors)) {
      return(<span className="fa fa-times"/>);
    }
  }

  executionButton(inProgress) {
    if(inProgress) {
      return(
        <button type="submit" className="sandbox-go-button" disabled>
          Starting... <span className="fa fa-gear fa-spin fa-fw" />
        </button>
      );
    }
    else {
      return(
        <button type="submit" className="sandbox-go-button">
          GO!
        </button>
      );
    }
  }

  lookBelowMessage(executions) {
    if(executions.length > 0) {
      return(
         <span>Now look below! <span className="fa fa-hand-o-down" /></span>
      );
    }
  }

  render() {
    const { appState, handleSubmit, load, pristine, reset, submitting } = this.props;
    return(
      <Form onSubmit={handleSubmit(this.onSubmit.bind(this))}>
        <h2>1. Write some code  <span className="fa fa-diamond"/></h2>
        <div className="sandbox-left">
          <div>
            <Field
              name="step_class_name"
              component={renderField}
              type="text"
              placeholder="Name of Class (must match class name below)"
              label="Step Class Name (must match below)" />
          </div>
        </div>
        <div>
          <Field
            name="description"
            component={renderField}
            type="text"
            placeholder="Describe your step"
            label="Description" />
        </div>
        <div>
          <label>Step Code (ruby) <span className="fa fa-diamond"/></label>
          <div>
            <Field className="sandbox-code-input" rows="30" cols="80" name="code" component="textarea" />
          </div>
        </div>
        <h2>2. Choose some Input Files  <span className="fa fa-file-o"/></h2>
        <div className="sandbox-component-container">
          <div>
            <Field
              name="input_files"
              component={renderDropzoneInput}
            />
          </div>
        </div>
        <div>
          <h2>3. Specify any parameters <span className="fa fa-key"/></h2>
          <div className="sandbox-component-container">
            <SandboxParameterForm onSubmit={this.handleSubmit} />
          </div>
        </div>
        <div>
          <h2>4. Ready... get set...  <span className="fa fa-flag-checkered"/></h2>
          <div className="sandbox-component-container padding-top-1rem">
            { this.executionButton(appState.sandboxPlaceholderCount >= 1) }
            { this.lookBelowMessage(appState.singleStepExecutions) }
          </div>
        </div>
      </Form>
    );
  }
}

const renderUploadFileList = (files) => {
  if(files && Array.isArray(files)) {
    return(
      <ul className="dropzone-file-list">
        { files.map((file, i) =>
          <li key={i} className="dropzone-file-list-item">
            <span className="fa fa-file-o"/> {file.name}
          </li>
        )}
      </ul>
    );
  } else {
    return(<div className="left-indent small-info"><span className="fa fa-hand-o-left"/> Please select some</div>);
  }
};

const renderDropzoneInput = (field) => {
  const files = field.input.value;
  return (
    <div className="dropzone-container">
      <div className="dropzone-widget">
        <Dropzone
          name={field.name}
          onDrop={( filesToUpload, e ) => field.input.onChange(filesToUpload)}
        >
          <div className="dropzone">Drop files here, or click to select files.</div>
          <div className="dropzone-icon-container">
            <span className="dropzone-icon fa fa-files-o"/>
          </div>
        </Dropzone>
        {field.meta.touched &&
          field.meta.error &&
          <span className="error">{field.meta.error}</span>}
      </div>
      <div className="dropzone-file-list">
        <div className="dropzone-file-list-title left-indent"><span className="fa fa-files-o"/> Files to upload ({files.length})</div>
        { renderUploadFileList(files) }
      </div>
    </div>
  );
};

const handleCodeChange = (e) => {
  console.log("value", e.target.value);
};

const renderField = ({ input, label, type, meta: { touched, error, warning } }) => (
  <div>
    <label>{label}</label>
    <div>
      <input className="sandbox-input-wide" {...input} placeholder={label} type={type}/>
      {touched && ((error && <span>{error}</span>) || (warning && <span>{warning}</span>))}
    </div>
  </div>
);

CodeForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  reset: PropTypes.func.isRequired,
  dispatch: PropTypes.func.isRequired,
  appState: PropTypes.object.isRequired
};

function mapStateToProps(state) {
  return {
    appState: state.appState,
    state: state
  };
}

const defaultData = {
  step_class_name: "SampleStep",
  description: "A sample step class",
  execution_parameters: {blargh: "haha"},
  code:
`class SampleStep < InkStep::ConversionStep

  def perform_step
    # put some logic here
    success!
  end

  def version
    "1.0"
  end

  def description
    "A sample step that doesn't do anything"
  end

  def self.human_readable_name
    "Sample step"
  end
end`
};

export default compose(
  connect(mapStateToProps),
  reduxForm(
    {form: "sandbox",
      initialValues: defaultData
    }, validateSandbox
  )
)(CodeForm);
