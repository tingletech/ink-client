import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import _  from "lodash";
import SmoothCollapse from 'react-smooth-collapse';

import * as actions from '../actions/authenticationActions';
import { downloadInputFile, downloadOutputFile, downloadInputZip, downloadsingleStepExecutionOutputZip, downloadsingleStepExecutionOutputFile } from '../businessLogic/fileLogic';
import { collapsesingleStepExecutionInputFileList, expandsingleStepExecutionInputFileList, collapsesingleStepExecutionOutputFileList, expandsingleStepExecutionOutputFileList } from '../actions/recipeActions';

export class SandboxExecutionFileList extends Component {

  handleOutputZipDownload = (e, standaloneStepExecutionId) => {
    e.preventDefault();
    const { appState, dispatch } = this.props;
    dispatch(downloadSandboxOutputZip(standaloneStepExecutionId, appState, dispatch));
  }

  handleOutputFileDownload = (e, filePath, standaloneStepExecutionId) => {
    e.preventDefault();
    const { appState, dispatch } = this.props;
    dispatch(downloadSandboxOutputFile(filePath, standaloneStepExecutionId, appState, dispatch));
  }

  handleInputZipDownload = (e, standaloneStepExecutionId) => {
    e.preventDefault();
    const { appState, dispatch } = this.props;
    dispatch(downloadSandboxInputZip(standaloneStepExecutionId, appState, dispatch));
  }

  handleInputFileDownload = (e, filePath, standaloneStepExecutionId) => {
    e.preventDefault();
    const { appState, dispatch } = this.props;
    dispatch(downloadSandboxInputFile(filePath, standaloneStepExecutionId, appState, dispatch));
  }

  handleToggle = (e, isInput, toExpand, singleStepExecutionId, recipeId) => {
    e.preventDefault();
    const { appState, dispatch } = this.props;
    if(isInput == true && toExpand == true) {
      dispatch(expandSandboxInputFileList(singleStepExecutionId, recipeId, appState));
    } else if (isInput == true) {
      dispatch(collapseSandboxInputFileList(singleStepExecutionId, recipeId, appState));
    } else if (toExpand == true) {
      dispatch(expandSandboxOutputFileList(singleStepExecutionId, recipeId, appState));
    } else {
      dispatch(collapseSandboxOutputFileList(singleStepExecutionId, recipeId, appState));
    }
  }

  renderFileLink(filePath, isOutput, id) {
    if(_.isNil(filePath) || _.isEmpty(filePath)) {
      return(
        <span className="small-info-text">[ no files ]</span>
      );
    }
    else if(isOutput === true) {
      return(
        <a href={`#id=${id}`} onClick={e => this.handleOutputFileDownload(e, filePath, id)}>{filePath} <span className="fa fa-download"/></a>
      );
    }
    else if(isOutput === false) {
      return(
        <a href={`#id=${id}`} onClick={e => this.handleInputFileDownload(e, filePath, id)}>{filePath} <span className="fa fa-download"/></a>
      );
    }
    else {
      return(<span className="small-info-text">[ no files ]</span>);
    }
  }

  renderZipLink(isOutput, id) {
    if(isOutput === true) {
      return(
        <a href={`#id=${id}`} onClick={e => this.handleOutputZipDownload(e, id)}>all files (zip) <span className="fa fa-download"/></a>
      );
    }
    else if(isOutput === false) {
      return(
        <a href={`#id=${id}`} onClick={e => this.handleInputZipDownload(e, id)}>all files (zip) <span className="fa fa-download"/></a>
      );
    }
    else {
      return(<span className="small-info-text">[ no files ]</span>);
    }
  }

  renderFileList(isOutput, fileList, singleStepExecutionId, collapsed) {
    if(_.isEmpty(fileList) || _.isNull(fileList)) {
      return(<span/>);
    }
    return(
      <ul className="file-list">
        <SmoothCollapse expanded={!collapsed}>
          { fileList.map((file) =>
            <li className="file-list-item monospace" key={file.path}>
              <span className="fa fa-hand-o-right small-info" />
              &nbsp;
              { this.renderFileLink(file.path, isOutput, singleStepExecutionId) } <span className="small-info-text">({ file.size })</span>
            </li>)
          }
        </SmoothCollapse>
      </ul>
    );
  }

  renderSandboxOutputLink(standaloneStepExecution) {
    if(!_.isNil(standaloneStepExecution.finished_at)) {
      return( this.renderZipLink(true, standaloneStepExecution.id) );
    }
    return(<span className="small-info">&nbsp;[ Not available ]</span>);
  }

  listExpandCollapseToggle(standaloneStepExecution, isInput) {
    if(isInput == true) {
      return(
        <a href="#" onClick={e => this.handleToggle(e, isInput, standaloneStepExecution.input_file_list_collapse, standaloneStepExecution.id, standaloneStepExecution.recipe_id)}>
          <span className={this.listIcon(standaloneStepExecution.input_file_list_collapse)}/>
          &nbsp;Input Files ({_.size(standaloneStepExecution.input_file_manifest)})
        </a>
      );
    } else {
      return(
        <a href="#" onClick={e => this.handleToggle(e, isInput, standaloneStepExecution.output_file_list_collapse, standaloneStepExecution.id, standaloneStepExecution.recipe_id)}>
          <span className={this.listIcon(standaloneStepExecution.output_file_list_collapse)}/>
          &nbsp;Output Files ({_.size(standaloneStepExecution.output_file_manifest)})
        </a>
      );
    }
  }

  listIcon(collapsed) {
    if(collapsed == true || _.isNil(collapsed)) {
      return "fa fa-caret-right";
    } else {
      return "fa fa-caret-down";
    }
  }

  render() {
    const { standaloneStepExecution } = this.props;
    return(
      <div>
        <div>
          {this.listExpandCollapseToggle(standaloneStepExecution, true)} <span className="silver">|</span> { this.renderZipLink(false, standaloneStepExecution.id) }
        </div>
        <div>{ this.renderFileList(false, standaloneStepExecution.input_file_manifest, standaloneStepExecution.id, standaloneStepExecution.input_file_list_collapse) }</div>
        <div>
          {this.listExpandCollapseToggle(standaloneStepExecution, false)} | { this.renderOutputLink(standaloneStepExecution) }
        </div>
        <div>{ this.renderFileList(true, standaloneStepExecution.output_file_manifest, standaloneStepExecution.id, standaloneStepExecution.output_file_list_collapse) }</div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return { appState: state.appState };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch),
    dispatch: dispatch
  };
}

SandboxExecutionFileList.propTypes = {
  standaloneStepExecution: PropTypes.object.isRequired,
  appState: PropTypes.object,
  dispatch: PropTypes.func
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SandboxExecutionFileList);
