import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import _  from "lodash";
import SmoothCollapse from 'react-smooth-collapse';

import * as actions from '../../actions/authenticationActions';
import { downloadLogFile, downloadSingleStepExecutionOutputFile, downloadSingleStepExecutionOutputZip, downloadSingleStepExecutionInputFile, downloadSingleStepExecutionInputZip } from '../../businessLogic/fileLogic';
import { expandSingleStepExecutionInputFileList, collapseSingleStepExecutionInputFileList, expandSingleStepExecutionOutputFileList, collapseSingleStepExecutionOutputFileList } from '../../actions/recipeActions';

export class ExecutionFileList extends Component {

  handleSingleStepInputFileDownload = (e, fileName) => {
    e.preventDefault();
    const { appState, dispatch, singleStepExecution } = this.props;
    dispatch(downloadSingleStepExecutionInputFile(fileName, singleStepExecution.id, appState, dispatch));
  }

  handleSingleStepExecutionInputZipDownload = (e) => {
    e.preventDefault();
    const { appState, dispatch, singleStepExecution } = this.props;
    dispatch(downloadSingleStepExecutionInputZip(singleStepExecution.id, appState, dispatch));
  }

  handleSingleStepOutputFileDownload = (e, fileName) => {
    e.preventDefault();
    const { appState, dispatch, singleStepExecution } = this.props;
    dispatch(downloadSingleStepExecutionOutputFile(fileName, singleStepExecution.id, appState, dispatch));
  }

  handleSingleStepExecutionOutputZipDownload = (e) => {
    e.preventDefault();
    const { appState, dispatch, singleStepExecution } = this.props;
    dispatch(downloadSingleStepExecutionOutputZip(singleStepExecution.id, appState, dispatch));
  }
  
  handleToggle = (e, isInput, toExpand, executionId) => {
    e.preventDefault();
    const { appState, dispatch } = this.props;
    if(isInput == true && toExpand == true) {
      dispatch(expandSingleStepExecutionInputFileList(executionId, appState));
    } else if (isInput == true) {
      dispatch(collapseSingleStepExecutionInputFileList(executionId, appState));
    } else if (toExpand == true) {
      dispatch(expandSingleStepExecutionOutputFileList(executionId, appState));
    } else {
      dispatch(collapseSingleStepExecutionOutputFileList(executionId, appState));
    }
  }

  renderFileLink(filePath, isOutput, id) {
    if(_.isNil(filePath) || _.isEmpty(filePath)) {
      return(
        <span className="small-info-text">[ no files ]</span>
      );
    }
    else if(isOutput === true) {
      return(
        <a href={`#id=${id}`} onClick={e => this.handleSingleStepOutputFileDownload(e, filePath, id)}>{filePath} <span className="fa fa-download"/></a>
      );
    }
    else if(isOutput === false) {
      return(
        <a href={`#id=${id}`} onClick={e => this.handleSingleStepInputFileDownload(e, filePath, id)}>{filePath} <span className="fa fa-download"/></a>
      );
    }
    else {
      return(<span className="small-info-text">[ no files ]</span>);
    }
  }

  renderZipLink(isOutput, id) {
    if(isOutput === true) {
      return(
        <a href={`#id=${id}`} onClick={e => this.handleSingleStepExecutionOutputZipDownload(e, id)}>all files (zip) <span className="fa fa-download"/></a>
      );
    }
    else if(isOutput === false) {
      return(
        <a href={`#id=${id}`} onClick={e => this.handleSingleStepExecutionInputZipDownload(e, id)}>all files (zip) <span className="fa fa-download"/></a>
      );
    }
    else {
      return(<span className="small-info-text">[ no files ]</span>);
    }
  }

  renderFileList(isOutput, fileList, executionId, collapsed) {
    if(_.isEmpty(fileList) || _.isNull(fileList)) {
      return(<span/>);
    }
    return(
      <ul className="file-list">
        <SmoothCollapse expanded={!collapsed}>
          { fileList.map((file) =>
            <li className="file-list-item monospace" key={file.path}>
              <span className="fa fa-hand-o-right small-info" />
              &nbsp;
              { this.renderFileLink(file.path, isOutput, executionId) } <span className="small-info-text">({ file.size })</span>
            </li>)
          }
        </SmoothCollapse>
      </ul>
    );
  }

  renderOutputLink(singleStepExecution) {
    if(!_.isNil(singleStepExecution.finished_at)) {
      return( this.renderZipLink(true, singleStepExecution.id) );
    }
    return(<span className="small-info">&nbsp;[ Not available ]</span>);
  }

  listExpandCollapseToggle(singleStepExecution, isInput) {
    if(isInput == true) {
      return(
        <a href="#" onClick={e => this.handleToggle(e, isInput, singleStepExecution.input_file_list_collapse, singleStepExecution.id)}>
          <span className={this.listIcon(singleStepExecution.input_file_list_collapse)}/>
          &nbsp;Input Files ({_.size(singleStepExecution.input_file_manifest)})
        </a>
      );
    } else {
      return(
        <a href="#" onClick={e => this.handleToggle(e, isInput, singleStepExecution.output_file_list_collapse, singleStepExecution.id)}>
          <span className={this.listIcon(singleStepExecution.output_file_list_collapse)}/>
          &nbsp;Output Files ({_.size(singleStepExecution.output_file_manifest)})
        </a>
      );
    }
  }

  listIcon(collapsed) {
    if(collapsed == true || _.isNil(collapsed)) {
      return "fa fa-caret-right";
    } else {
      return "fa fa-caret-down";
    }
  }

  render() {
    const { singleStepExecution } = this.props;
    return(
      <div>
        <div>
          {this.listExpandCollapseToggle(singleStepExecution, true)} <span className="silver">|</span> { this.renderZipLink(false, singleStepExecution.id) }
        </div>
        <div>{ this.renderFileList(false, singleStepExecution.input_file_manifest, singleStepExecution.id, singleStepExecution.input_file_list_collapse) }</div>
        <div>
          {this.listExpandCollapseToggle(singleStepExecution, false)} | { this.renderOutputLink(singleStepExecution) }
        </div>
        <div>{ this.renderFileList(true, singleStepExecution.output_file_manifest, singleStepExecution.id, singleStepExecution.output_file_list_collapse) }</div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return { appState: state.appState };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch),
    dispatch: dispatch
  };
}

ExecutionFileList.propTypes = {
  singleStepExecution: PropTypes.object.isRequired,
  appState: PropTypes.object,
  dispatch: PropTypes.func
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ExecutionFileList);
