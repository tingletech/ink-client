const validateSandbox = values => {
  const errors = {};
    if (!values.step_class_name) {
      errors.step_class_name = 'Required';
    }
    if (!values.description) {
      errors.description = 'Required';
    }
    if (!values.input_files) {
      errors.input_files = 'Required';
    }
  return errors;
};

export default validateSandbox;
