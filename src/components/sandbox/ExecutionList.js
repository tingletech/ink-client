import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import * as actions from '../../actions/authenticationActions';
import _ from 'lodash';

import ExecutionDetail from './ExecutionDetail';
import ExecutionPlaceholder from '../ExecutionPlaceholder';

import * as eventActions from '../../constants/PusherActions';
import AlertList from '../../components/AlertList';
import { subscribe, unsubscribe } from 'pusher-redux';

export class ExecutionList extends Component {

  renderPlaceholders(count) {
    if(count < 0 || _.isEmpty(count)) { return null; }
    return(
      <div className="placeholder-container">
        {[...Array(count)].map((x, i) =>
          <ExecutionPlaceholder key={i + _.now()} />
        )}
      </div>
    );
  }

  renderAlerts(alerts) {
    if(alerts.length > 0) {
      return(
        <div>
          <div>Here is how it went!</div>
          <AlertList alerts={alerts} pageName="sandbox" />
        </div>
        );
    }
  }

  renderExecutionResults() {
    let { appState } = this.props;
    let { alerts } = appState;

    let executionPlaceholderCount = appState.sandboxExecutionPlaceholderCount;
    if(executionPlaceholderCount > 0 || appState.singleStepExecutions.length > 0)
      return(
        <div>
          {this.renderAlerts(alerts)}
          { this.renderPlaceholders(executionPlaceholderCount) }

          {appState.singleStepExecutions.map((execution, index) =>
            <ExecutionDetail singleStepExecution={execution} key={index} />)}
        </div>
       );
     else {
       return(
         <div>
           <span className="fa fa-4x fa-magic grey" />
         </div>
       );
     }
  }

  render() {
    let { appState, sandboxExecutionPlaceholderCount } = this.props;

    return(
      <div>
        <h2>5. See the results in real time!  <span className="fa fa-magic"/></h2>
        <div className="sandbox-component-container">
          {this.renderExecutionResults()}
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return { appState: state.appState };
}

function mapDispatchToProps(dispatch) {
  return {
    eventActions: bindActionCreators(eventActions, dispatch),
    dispatch: dispatch
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ExecutionList);
