import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import _  from "lodash";

import SmoothCollapse from 'react-smooth-collapse';
import TimeAgo from 'react-timeago';

import ExecutionFileList from './ExecutionFileList';

import * as actions from '../../actions/authenticationActions';
import * as recipeActions from '../../actions/recipeActions';

import { downloadSingleStepExecutionLogFile, downloadCodeFile } from '../../businessLogic/fileLogic';

export class ExecutionDetail extends Component {

  handleLogFileDownload = (e, fileName) => {
    e.preventDefault();
    const { appState, dispatch, singleStepExecution } = this.props;
    dispatch(downloadSingleStepExecutionLogFile(fileName, singleStepExecution.id, appState, dispatch));
  }

  handleCodeFileDownload = (e, fileName) => {
    e.preventDefault();
    const { appState, dispatch, singleStepExecution } = this.props;
    dispatch(downloadCodeFile(fileName, singleStepExecution.id, appState, dispatch));
  }

  renderFinishedTitle() {
    let { singleStepExecution } = this.props;
    return(<span>Finished processing <TimeAgo date={singleStepExecution.finished_at} /> </span>);
  }

  renderInProgressTitle() {
    let { singleStepExecution } = this.props;
    if(_.isNil(singleStepExecution.executed_at)) {
      return(<span>Waiting to be processed (submitted <TimeAgo date={singleStepExecution.created_at} />) {singleStepExecution.executed_at}</span>);
    }
    return(<span>Processing (started <TimeAgo date={singleStepExecution.executed_at} />)</span>);
  }

  renderLinkText(file_name) {
    if(_.isNil(file_name)) {
      return;
    }
    return(<span>{file_name} <span className="fa fa-download"/></span>);
  }

  executionResult() {
    let { singleStepExecution } = this.props;
    if(_.isNil(singleStepExecution.finished_at)) {
      return(<p><span className="fa fa-gear fa-spin fa-fw" />{this.renderInProgressTitle()}</p>);
    }
    return(<p><span className="fa fa-check" /> {this.renderFinishedTitle()}</p>);
  }

  listIcon(collapsed) {
    if(collapsed == true || _.isNil(collapsed)) {
      return "fa fa-caret-right";
    } else {
      return "fa fa-caret-down";
    }
  }

  successClass(execution_errors, successful) {
    if(successful === true) {
      return("success success-background");
    }
    else if((!_.isNil(execution_errors) && !_.isEmpty(execution_errors)) || successful === false) {
      return("fail fail-background");
    }
    return("");
  }

  renderIcon(singleStepExecution) {
    if(singleStepExecution.successful === true) {
      return(<span className="fa fa-check"/>);
    }
    else if(singleStepExecution.in_progress === true) {
      return(<span className="fa fa-gear fa-spin fa-2x fa-fw step-processing-centered" />);
    }
    else if(_.isNil(singleStepExecution.executed_at)) {
      return(<span className="small-info-text fa fa-question"/>);
    }
    else if((!_.isNil(singleStepExecution.execution_errors) && !_.isEmpty(singleStepExecution.execution_errors)) || singleStepExecution.successful === false) {
      return(<span className="fa fa-times"/>);
    }
  }

  renderCodeLink(singleStepExecution) {
    let filePath = singleStepExecution.code_file_name;
    if(!_.isNil(filePath)) {
      return(
        <a href="#" onClick={e => this.handleCodeFileDownload(e, filePath, singleStepExecution.id)}>{filePath} <span className="fa fa-download"/></a>
      );
    }
    return(
      <span className="small-info-text">---</span>
    );
  }


  renderLogLink(filePath, singleStepExecutionId, finishedAt) {
    if(!_.isNil(filePath)) {
      return(
        <a href="#" onClick={e => this.handleLogFileDownload(e, filePath, singleStepExecutionId)}>{filePath} <span className="fa fa-download"/></a>
      );
    }
    return(
      <span className="small-info-text">---</span>
    );
  }

  renderLink(filePath, singleStepExecutionId, finishedAt) {
    if(!_.isNil(filePath)) {
      return(
        <a href="#" onClick={e => this.handleOutputFileDownload(e, filePath, singleStepExecutionId)}>{filePath} <span className="fa fa-download"/></a>
      );
    }
    return(
      <span className="small-info-text">---</span>
    );
  }

  renderProcessLogLink(singleStepExecution) {
    if(_.isNil(singleStepExecution.finished_at)) {
      return null;
    }
    let processLogLocation = singleStepExecution.process_log_location;
    return(
      <div>
        <span className="fa fa-hdd-o"/> Log: { this.renderLogLink(processLogLocation, singleStepExecution.id, singleStepExecution.finished_at) }
      </div>
    );
  }

  renderErrors(singleStepExecution) {
    let errors = singleStepExecution.execution_errors;

    if(_.isNil(singleStepExecution.finished_at)) {
      errors = <span/>;
    }
    else if(_.isNil(errors) || _.isEmpty(errors)) {
      errors = <span className="small-info-text">none</span>;
    }

    return(
      <div>Errors: <span className="fail">{ errors }</span></div>
    );
  }

  renderNotes(singleStepExecution) {
    let notes = singleStepExecution.notes;

    if(_.isNil(singleStepExecution.finished_at)) {
      notes = <span/>;
    }
    else if(_.isNil(notes) || _.isEmpty(notes)) {
      notes = <span className="small-info-text">none</span>;
    }

    return(
      <div>Notes: { notes }</div>
    );
  }

  renderOutput(singleStepExecution) {
    if(_.isNil(singleStepExecution.finished_at) && _.isNil(singleStepExecution.output_file_manifest)) {
      return(<span className="fa fa-hourglass-o small-info"/>);
    }
    return(
      <div>
        { this.renderCodeLink(singleStepExecution) }
        { this.renderErrors(singleStepExecution) }
        { this.renderNotes(singleStepExecution) }
        { this.renderProcessLogLink(singleStepExecution) }
      </div>
    );
  }

  outputRowClasses(singleStepExecution) {
    if(_.isNil(singleStepExecution.finished_at) && _.isNil(singleStepExecution.output_file_manifest)) {
      return("step-execution-result-item centered");
    }
    return("step-execution-result-item");
  }

  renderVersion(version) {
    if(_.isEmpty(version)) {
      return;
    }
    return(
      <div><span className="small-info-text">version {version}</span></div>
    );
  }

  renderExecutionParameters(executionParameters) {
    if(_.isNil(executionParameters) || _.isEmpty(executionParameters)) {
      return(
        <div>
          <span className="small-info-text">No parameters</span>
        </div>
      );
    }
    return(
      <div className="step-execution-result-parameters">
        {executionParameters && Object.keys(executionParameters).map(function(key, idx) {
            return <div className="param-row" key={idx}><div className="param-key"><span className="fa fa-key"/> {key}</div><div className="param-value">{executionParameters[key]}</div></div>;
        }.bind(this))}
      </div>
    );
  }

  renderStepShorthand(stepName) {
    let { singleStepExecution } = this.props;
    let className = "";
    if(singleStepExecution.in_progress === true) {
      className = "bold";
    }
    return(
      <div>
        <div>{singleStepExecution.step_class_name}</div>
      </div>
    );
  }

  render() {
    const { singleStepExecution } = this.props;

    return(
      <div  className="single-step-execution-results-wrapper">
        <div className="execution-result-border">
          {this.executionResult()}

          <ExecutionFileList key={singleStepExecution.id}
            singleStepExecution={singleStepExecution}
            inputFileManifest={singleStepExecution.input_file_manifest}
          />
        </div>
        <div className={`step-execution-result-item result-icon centered ${this.successClass(singleStepExecution.execution_errors, singleStepExecution.successful)}`}>{this.renderIcon(singleStepExecution)}</div>
        <div className="step-execution-result-item">
          {this.renderStepShorthand(singleStepExecution.step_class_name)}
          {this.renderVersion(singleStepExecution.version)}
          {this.renderExecutionParameters(singleStepExecution.execution_parameters)}
        </div>
        <div className={this.outputRowClasses(singleStepExecution)}>
          { this.renderOutput(singleStepExecution) }
        </div>
      </div>
    );
  }
}

ExecutionDetail.propTypes = {
  dispatch: PropTypes.func,
  singleStepExecution: PropTypes.object.isRequired,
  appState: PropTypes.object
};

function mapStateToProps(state) {
  return { appState: state.appState };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch),
    dispatch: dispatch
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ExecutionDetail);
