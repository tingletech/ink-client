import React, { Component, PropTypes } from 'react';
import * as AlertTypes from '../constants/AlertTypes';

const divWidget = (props) => {

  let content = props.content;
  let type = props.type;

  if(type === AlertTypes.ERROR) {
    return (
      <div className="alert alert--error">
        <span className="fa fa-warning right-space" aria-hidden="true"/> {content}
      </div>
    );
	} else if(type === AlertTypes.WARNING) {
    return (
      <div className="alert alert--warning">
        <span className="fa fa-warning right-space" aria-hidden="true"/> {content}
      </div>
    );
  } else if (type === AlertTypes.SUCCESS) {
    return (
      <div className="alert alert--success">
        <span className="fa fa-check right-space" aria-hidden="true"/> {content}
      </div>
    );
  } else {
    return (
      <div className="alert alert--info">
        <span className="fa fa-info-circle right-space" aria-hidden="true"/> {content}
      </div>
    );
  }
};

divWidget.propTypes = {
  type: PropTypes.string.isRequired,
  content: PropTypes.string.isRequired
};

export default divWidget;
