/////////////////// show recipe view ///////////////////

export const SHOW_RECIPE_STEP_VIEW = "SHOW_RECIPE_STEP_VIEW";
export const SHOW_RECIPE_HISTORY_VIEW = "SHOW_RECIPE_HISTORY_VIEW";
export const SHOW_PRESET_VIEW = "SHOW_PRESET_VIEW";
export const SHOW_PARAMETER_FORM = "SHOW_PARAMETER_FORM";
