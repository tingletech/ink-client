import React, { Component, PropTypes } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { browserHistory, Link } from 'react-router';
import _ from 'lodash';

import * as actions from '../../actions/adminActions.js';

import Header from '../../components/header/Header';

import AccountsList from '../../components/admin/AccountsList';
import StatusReport from '../../components/admin/StatusReport';
import AlertList from '../../components/AlertList';
import { setAlert } from '../../actions/actions_helper';

class AdminDashboardPage extends Component {
  static propTypes = {
    actions: PropTypes.object.isRequired,
    appState: PropTypes.object.isRequired,
    dispatch: PropTypes.func.isRequired
  };

  componentWillMount = () => {
    this.checkAuth();
  }

  componentDidMount = () => {
    const { appState, dispatch } = this.props;
    this.checkAuth();
    dispatch(actions.getAllAccounts(appState));
    dispatch(actions.getStatusReport(appState));
    dispatch(actions.getAvailableStepGems(appState));
  }

  checkAuth() {
    const { appState, dispatch } = this.props;
    const { session } = appState;

    if(_.isNull(session.account)) {
      dispatch(setAlert("Please sign in"));
      browserHistory.push('/signin');
      return;
    } else if(session.account.admin != true) {
      dispatch(setAlert("Authorised accounts only"));
      browserHistory.push('/');
      return;
    }
  }

  renderStatusReport(appState) {
    return(
      <StatusReport
        statusReport={appState.admin.statusReport}
        getStatusReportInProgress={appState.admin.getStatusReportInProgress}
        session={appState.session}
      />
    );
  }

  renderStepGemItem(gem) {
    return(
      <div className="admin-gem-item" key={gem.name}>
        <div className="admin-gem-name">
          <span className="fa fa-diamond"/> <code>{gem.name}</code>
        </div>
        <div className="gem--blue gem__admin-version">Version: <code>{gem.version.version}</code></div>
        <div className="gem--blue gem__admin-git-version">Git version: <code>{gem.git_version}</code></div>
        <div className="gem--blue gem__admin-repo"><span className="fa fa-git"/> Git repo: <code>{gem.repo}</code></div>
        <div className="step-classes-container">
          {gem.step_classes.map(klass => this.renderStepClassItem(klass))}
        </div>
      </div>
    );
  }

  renderStepClassItem(klass) {
    return (
      <div key={klass.name}>
        <div className="step--blue admin-step__title">{klass.name}</div>
        <div className="step--blue step__admin-description">{klass.description}</div>
      </div>
    );
  }

  renderAvailableStepGems(appState) {
    if(_.isEmpty(appState.admin.availableStepGems) || _.isNil(appState.admin.availableStepGems)) {
      return(
        <div className="loading-centered">
          <div><span className="fa fa-cog fa-spin fa-3x fa-fw" /></div>
          <div><span className="small-info">loading...</span></div>
        </div>
      );
    }
    return(
      appState.admin.availableStepGems.map(gem => this.renderStepGemItem(gem))
    );
  }

  renderAccountList(appState) {
    if(_.isEmpty(appState.admin.accounts) || appState.admin.getAccountsInProgress == true) {
      return(
        <div className="loading-centered">
          <div><span className="fa fa-cog fa-spin fa-3x fa-fw" /></div>
          <div><span className="small-info">loading...</span></div>
        </div>
      );
    }
    else {
      return(
        <div>
          <AccountsList
            accounts={appState.admin.accounts}
            getAccountsInProgress={appState.admin.getAccountsInProgress}
            session={appState.session}
          />
        </div>
      );
    }
  }

  pageContent(appState) {
    return(
      <div className="recipe-detail-view-container">
        <div>
          <div>
            <h4>status report</h4>
            { this.renderStatusReport(appState) }
          </div>
          <div>
            <h4>available step gems</h4>
            { this.renderAvailableStepGems(appState) }
          </div>
        </div>
        <div>
          <h4>accounts</h4>
          { this.renderAccountList(appState) }
        </div>
      </div>
    );
  }

  render() {
    const { appState } = this.props;

    return (
      <div>
        <Header
          appState={appState}
        />
        <div className="content-container">
          <div className="breadcrumb-container">
            <Link to="/" className="breadcrumb">Home</Link>
            <span className="breadcrumb-divider">&gt;&gt;</span>
            <Link to="/admin/accounts" className="breadcrumb"><span className="fa fa-accounts-o"/> Accounts</Link>
          </div>

          <AlertList
            alerts={appState.alerts} />
          <h1>Admin dashboard</h1>
          {this.pageContent(appState)}
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    appState: state.appState,
    alerts: PropTypes.array.isRequired,
    recipes: PropTypes.array.isRequired
  };
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(actions, dispatch),
    dispatch: dispatch
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AdminDashboardPage);
